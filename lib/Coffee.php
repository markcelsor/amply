<?php
class Coffee
{
  public $id;
  public $userID;
  public $fullname;

  public function getByProfileID($id) {

    $db = $GLOBALS['db'];
    $query = "SELECT id, user_id, created FROM coffees WHERE user_id = " . intval($id);
    if ($result = $db->query($query)) {
      while($row = $result->fetch_object())
      {
        $coffee = new Coffee();
        $coffee->id = $row->id;
        $coffee->userID = $row->user_id;
        $coffee->created = $row->created;
        $coffees[] = $coffee;
      }
    }
		return $coffees;
  }
  public function insert() {
    $db = $GLOBALS['db'];

		$query = "INSERT INTO coffees (user_id, created) VALUES (" . intval($this->userID) . ", NOW())";

    if ($result = $db->query($query)) {
      $this->id = $db->insert_id;
      return TRUE;
    }
		return FALSE;
  }
}
?>
