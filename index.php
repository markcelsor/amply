<?php
  ini_set('error_reporting', E_ALL);
  include "lib/app.php";
  $pageData = handleRequestAndReturnPageData($_GET);

?><!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Coffee Tracker</title>
    <link href="/assets/style.css" rel="stylesheet" type="text/css">
  </head>
  <body>
    <?php if ($pageData['message'] != '') { ?>
    <div class="messages"><?php echo $pageData['message'] ?></div>
    <?php } ?>
    <header>
      <form action="/" method="get">
        <input type="hidden" name="controller" value="select_profile" />
        <select name="profile_id" onchange="this.form.submit()">
          <option value="" disabled <?php
          if($_SESSION['profile_id'] == '' || $pageData['view'] == 'home') {
            echo ' selected';
          }
          ?>>Select User</option>
          <?php foreach(Profile::getList() as $profile) { ?>
          <option value="<?php echo $profile->id; ?>" <?php
          if($profile->id == $_SESSION['profile_id'] && $pageData['view'] != 'home') {
            echo ' selected';
          }
          ?>><?php echo $profile->username; ?></option>
          <?php } ?>
        </select>
      </form>
      <nav>
        <ul>
          <li><a href="/?controller=profile">Profile</a></li>
          <li><a href="/?controller=coffees">Coffees</a></li>
        </ul>
      </nav>
    </header>
    <section>
      <?php if ($pageData['view'] == 'home') { ?>
      <article class="home">
        <h1>Select a Profile to Begin</h1>
      </article>
      <?php } ?>
      <?php if ($pageData['view'] == 'profile') { ?>
      <article class="profile">
        <h1><?php echo $pageData['profile']->username ?>'s Profile</h1>
        <form action="/" method="get">
          <input type="hidden" name="controller" value="update_profile" />
          <div class="col-half">
            <label for="username">Username:</label><br />
            <input type="text" id="username" name="username" value="<?php echo $pageData['profile']->username ?>" />
          </div>
          <div class="col-half">
            <label for="fullname">Full Name:</label><br />
            <input type="text" id="fullname" name="fullname" value="<?php echo $pageData['profile']->fullname ?>" />
          </div>
          <input type="submit" value="Save" />
        </form>
      </article>
      <?php } ?>
      <?php if ($pageData['view'] == 'coffees') { ?>
      <article class="coffees">
        <h1><?php echo $pageData['profile']->username ?>'s Coffees</h1>
        <ul>
          <?php foreach($pageData['coffees'] as $coffee) { ?>
            <li><?php echo date("n/j/y g:ia", strtotime($coffee->created)) ?></li>
          <?php } ?>
        </ul>
        <form action="/" method="get">
          <input type="hidden" name="controller" value="add_coffee" />
          <input type="submit" value="Add a coffee">
        </form>
      </article>
      <?php } ?>
    </section>
  </body>
</html>
