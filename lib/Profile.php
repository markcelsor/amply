<?php
class Profile
{
  public $id;
  public $username;
  public $fullname;

  public function getByID($id) {
    $db = $GLOBALS['db'];
    $query = "SELECT id, username, fullname FROM profiles WHERE id = " . intval($id);

    if ($row = $db->query($query)->fetch_object()) {
        $profile = new Profile();
        $profile->id = $row->id;
        $profile->username = $row->username;
        $profile->fullname = $row->fullname;
        return $profile;
    } else {
      return FALSE;
    }
  }
  public function getList() {

    $db = $GLOBALS['db'];
    $query = "SELECT id, username, fullname FROM profiles";

    if ($result = $db->query($query)) {
      while($row = $result->fetch_object())
      {
        $profile = new Profile();
        $profile->id = $row->id;
        $profile->username = $row->username;
        $profile->fullname = $row->fullname;
        $profiles[] = $profile;
      }
    }
    return $profiles;
  }
  public function update() {
    $db = $GLOBALS['db'];

    $query = "UPDATE profiles SET " .
      "username = '" . $db->escape_string(substr($this->username, 0, 255)) . "', " .
      "fullname = '" . $db->escape_string(substr($this->fullname, 0, 255)) . "' " .
      "WHERE id = " . intval($this->id);

    if ($result = $db->query($query)) {
      return TRUE;
    }
    return FALSE;
  }
}
?>
