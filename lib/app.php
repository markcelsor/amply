<?php
include 'Profile.php';
include 'Coffee.php';
session_start();
$GLOBALS['db'] = new mysqli('localhost', 'root', '', 'amply');
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

/**
 * Routing function
 */
function handleRequestAndReturnPageData()
{
  $pageData['message'] = '';
  $pageData['view'] = 'home';

    switch ($_REQUEST['controller']) {
      case 'add_coffee':
        $pageData = addCoffee();
        break;
      case 'coffees':
        $pageData = coffees();
        break;
      case 'profile':
        $pageData = profile();
        break;
      case 'select_profile':
        $pageData = selectProfile();
        break;
      case 'update_profile':
        $pageData = updateProfile();
        break;
  }

  return $pageData;
}

/**
 * Controller functions
 */
 function addCoffee() {
   $data['message'] = '';
   $data['view'] = 'home';

   if($_SESSION['profile_id'] != '') {
     $profile = Profile::getByID($_SESSION['profile_id']);
     if($profile) {
       $coffee = new Coffee();
       $coffee->userID = $profile->id;
       if($coffee->insert()) {
         $data['message'] = 'Coffee added!';
       } else {
         $data['message'] = 'Error: coffee was not added!';
       }
       $data['profile'] = $profile;
       $data['coffees'] = Coffee::getByProfileID($profile->id);
       $data['view'] = 'coffees';
     }
   }
   return $data;
 }

 function coffees() {
   $data['message'] = '';
   $data['view'] = 'home';

   if($_SESSION['profile_id'] != '') {
     $profile = Profile::getByID($_SESSION['profile_id']);
     if($profile) {
       $data['profile'] = $profile;
       $data['message'] = '';
       $data['view'] = 'coffees';
       $data['coffees'] = Coffee::getByProfileID($profile->id);
     }
   }
   return $data;
 }

 function profile() {
   $data['message'] = '';
   $data['view'] = 'home';

   if($_SESSION['profile_id'] != '') {
     $profile = Profile::getByID($_SESSION['profile_id']);
     if($profile) {
       $data['view'] = 'profile';
       $data['profile'] = $profile;
     }
   }
   return $data;
 }

function selectProfile() {
  $_SESSION['profile_id'] = '';

  $data['message'] = 'Profile not found!';
  $data['view'] = 'home';

  if($_REQUEST['profile_id'] != '') {
    $profile = Profile::getByID($_REQUEST['profile_id']);
    if($profile) {
      $_SESSION['profile_id'] = $profile->id;
      $data['message'] = 'Profile selected!';
      $data['view'] = 'profile';
      $data['profile'] = $profile;
    }
  }
  return $data;
}

function updateProfile() {
  $data['message'] = '';
  $data['view'] = 'home';

  if($_SESSION['profile_id'] != '') {
    $profile = Profile::getByID($_SESSION['profile_id']);
    if($profile) {
      $profile->username = $_REQUEST['username'];
      $profile->fullname = $_REQUEST['fullname'];
      if($profile->update()) {
        $data['message'] = 'Profile updated!';
      } else {
        $data['message'] = 'Error: profile was not saved!';
      }
      $data['view'] = 'profile';
      $data['profile'] = $profile;
    }
  }
  return $data;
}

?>
