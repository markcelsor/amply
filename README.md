## Amply Coffee Tracker Sample App
Mark Celsor mark@celsor.com

Hello, I decided to build this application in PHP breaking it up in a simple, MVC inspired approach but steered clear of using any frameworks or unneeded complexity.

### index.php
This file contains all the html and simple conditional logic for displaying the views. It also contains the apps one line of Javascript to handle the select change.

### lib/app.php
This contains some set up and a routing function at the top followed by controller functions.

### lib/Coffee.php and lib/Profile.php
These are the model classes with methods to interact with the database. A more robust solution would have the rest of the CRUD methods and some testing and validation functionality, but for this example I just stuck to what was needed for the wireframes.

### assets/style.css
You will just find some very styling here. I did not go crazy.

### database.sql
This is a create script for the profiles and coffees tables with a little bit of seed data for profiles.
